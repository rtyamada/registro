import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {
  //cria uma lista local de teste
  //  lista = [
  //  {nome: 'Maria das Dores', email: 'md@email.com', idade:21},
  //  {nome: 'Maria das Graças', email: 'mg@email.com', idade:23},
  //  {nome: 'Maria do Rosario', email: 'mr@email.com', idade:25}
  // ];

  lista: Observable<any[]>;

  constructor(
    private alert: AlertController,
    private db: AngularFirestore
  ) { }

  ngOnInit() {
      this.lista = this.db.collection('pessoa').valueChanges();
  }

  async createUser() {
    const alert = await this.alert.create({
      subHeader: 'Digite os dados do usuario',
      inputs: [
        {
          type: 'text',
          name: 'nome',
          placeholder: 'Nome'
        },
        {
          type: 'email',
          name: 'email',
          placeholder: 'Email'
        },
        {
          type: 'number',
          name: 'idade',
          placeholder: 'Idade'
        }
      ],
      buttons: [
        { text: 'Cancelar', role: 'cancel' },
        {
          text: 'Enviar',
          handler: data => this.send(data)
        }
      ]
    });

      alert.present();
  }
  private send(data){
    this.db.collection('pessoa').add(data);

  }
}
